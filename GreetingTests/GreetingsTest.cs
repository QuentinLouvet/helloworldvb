﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace GreetingTests
{
    [TestClass]
    public class GreetingsTest
    {
        [TestMethod]
        public void HelloWorldTest()
        {
            Assert.AreEqual("Hello World !", Courtesy.Greetings.HelloWorld());
            Assert.AreEqual("Hello Regis !", Courtesy.Greetings.SayHello("Regis"));
        }
    }
}
