﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.HelloWorldButton = New System.Windows.Forms.Button()
        Me.HelloBen = New System.Windows.Forms.Button()
        Me.HelloWhoButton = New System.Windows.Forms.Button()
        Me.HelloWhoTextBox = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.HelloWhoLabel = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'HelloWorldButton
        '
        Me.HelloWorldButton.Location = New System.Drawing.Point(12, 12)
        Me.HelloWorldButton.Name = "HelloWorldButton"
        Me.HelloWorldButton.Size = New System.Drawing.Size(120, 28)
        Me.HelloWorldButton.TabIndex = 0
        Me.HelloWorldButton.Text = "Say Hello World"
        Me.HelloWorldButton.UseVisualStyleBackColor = True
        '
        'HelloBen
        '
        Me.HelloBen.Location = New System.Drawing.Point(12, 185)
        Me.HelloBen.Name = "HelloBen"
        Me.HelloBen.Size = New System.Drawing.Size(87, 30)
        Me.HelloBen.TabIndex = 1
        Me.HelloBen.Text = "Hello Ben"
        Me.HelloBen.UseVisualStyleBackColor = True
        '
        'HelloWhoButton
        '
        Me.HelloWhoButton.Location = New System.Drawing.Point(188, 185)
        Me.HelloWhoButton.Name = "HelloWhoButton"
        Me.HelloWhoButton.Size = New System.Drawing.Size(100, 30)
        Me.HelloWhoButton.TabIndex = 2
        Me.HelloWhoButton.Text = "Hello who ?"
        Me.HelloWhoButton.UseVisualStyleBackColor = True
        '
        'HelloWhoTextBox
        '
        Me.HelloWhoTextBox.Location = New System.Drawing.Point(188, 157)
        Me.HelloWhoTextBox.Name = "HelloWhoTextBox"
        Me.HelloWhoTextBox.Size = New System.Drawing.Size(100, 22)
        Me.HelloWhoTextBox.TabIndex = 3
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 97)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(143, 17)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "We are going to say :"
        '
        'HelloWhoLabel
        '
        Me.HelloWhoLabel.AutoSize = True
        Me.HelloWhoLabel.Location = New System.Drawing.Point(185, 97)
        Me.HelloWhoLabel.Name = "HelloWhoLabel"
        Me.HelloWhoLabel.Size = New System.Drawing.Size(0, 17)
        Me.HelloWhoLabel.TabIndex = 5
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(294, 249)
        Me.Controls.Add(Me.HelloWhoLabel)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.HelloWhoTextBox)
        Me.Controls.Add(Me.HelloWhoButton)
        Me.Controls.Add(Me.HelloBen)
        Me.Controls.Add(Me.HelloWorldButton)
        Me.Name = "Form1"
        Me.Text = "Hello World"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents HelloWorldButton As Button
    Friend WithEvents HelloBen As Button
    Friend WithEvents HelloWhoButton As Button
    Friend WithEvents HelloWhoTextBox As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents HelloWhoLabel As Label
End Class
