﻿Imports Courtesy

Public Class Form1
    Private Sub HelloWorldButton_Click(sender As Object, e As EventArgs) Handles HelloWorldButton.Click
        MessageBox.Show(Greetings.HelloWorld())
    End Sub

    Private Sub HelloBen_Click(sender As Object, e As EventArgs) Handles HelloBen.Click
        HelloWhoLabel.Text = Greetings.SayHello("Ben")
    End Sub

    Private Sub HelloWhoButton_Click(sender As Object, e As EventArgs) Handles HelloWhoButton.Click
        HelloWhoLabel.Text = Greetings.SayHello(HelloWhoTextBox.Text)
        If HelloWhoTextBox.Text < 0 Then
            HelloWhoLabel.Text = Greetings.SayHello("Nobody")
        End If
    End Sub
End Class
